# Git Service Use Cases & User Stories

This project contains use cases, user stories, and related documents for the practical evaluation of service candidates.

See the [Contribution Guide](CONTRIBUTING.md) for information on using these projects.

Committee meeting notes and project status can be found in the [Git Committee Notes](https://gitlab-beta.engr.illinois.edu/git-solutions-committee/git-committee-notes) project.

## Project Structure

### Proposals

Boilerplate for campus service proposals.

### Use Cases

Formal descriptions of features or requirements and their evaluation criteria, beginning with a user story. Templates are available in the Template subdirectory.

One use case per file.

### User Stories

Anecdotal service requirements. At minimum, each user story should contain a subject and action. User stories should contain a success indicator and rationale. If the level of need is known, user stories may use the MoSCoW scale (i.e., Must, Should, Could, Won't) as a general indicator.

User stories are arranged topically by file, with each file containing several subcategories, and each subcategory containing one or more user story.
