# Issue Trackers

## Permissions & Access

In an ideal case, private repositories could still expose a public issue tracker, allowing users to submit bug reports and feature requests, and track progress on existing issues. (This feature is available on Bitbucket.)

Most of our projects are heavily centered in public engagement or educational outreach, especially in grade schools – both local and across the country. This means that many of our typical users aren’t connected to the university in any way, and therefore wouldn’t be able to submit or track tickets if the system required a University NetID to access. For example, the support.education.illinois.edu and support.uillinois.edu ticketing systems both have an email interface that updates the ticket submitter on its status, but interacting with the ticket often requires logging in (with a NetID).

We would love to use a ticketing system that integrated with the campus’ LDAP, both for our own authentication and for any on-campus users (ticket submitters or managers) we have. However, if the system didn’t allow someone unconnected to the university to easily submit and track a ticket, it wouldn’t be a viable option for most of our projects. (Off the top of my head, I can only think of one project where we might want non-campus users to be able to manage tickets, so that’s much less of a concern.)

## Project Management

Our issue tracker is our main project management tool...
* The issue tracker must support arbitrary labels (not just a stock selection as on bitbucket)
* The issue tracker should support milestones with visible descriptions and due dates.

The issue tracker should support project management workflows through tools like Kanban boards, allowing us to prioritize monthly tasks. These could be native tools or third-party plugins.

The ideal solution could support project-level task management by allowing prioritization across repo issue trackers.
