# Version Control & Related Tools

## Build Tools & Release Management

Project Technical Leads should have the ability to enforce coding standards through hook-triggered linting tools and test runners, as well as automating builds and release workflows.

We use Jenkins/Travis/TeamCity for automated testing, continuous integration, and release management. We should be able to connect our project repos to our build server via webhooks.

We are considering trying to run a local maven repository -- probably Nexus http://books.sonatype.com/nexus-book/reference/index.html -- though there are other options too. Is there already a repository manager running that we could use? *Engineering IT support ticket 119227*

## Code Review Tools

Our developers frequently seek feedback from peers, both internal and around campus. We must have a convenient mechanism for code review, discussion, and approval, similar to GitHub's pull request and comment feature.

People should be able to review and comment from a web interface so they don't have to clone the repository.

Teams with open projects must be able to accept contributions from individuals, even if the individual does not have write access to the project.

 We use git as part of our publishing workflow. Merge requests could support approval signoff, similar to Bitbucket.

## Large File Support

Our development project contains large binary assets that are still under active development. This solution should have an option for graceful binary management.

Several of our research groups work with large datasets or generate large binary files as a primary function of their work. We try to avoid tracking those files in version control, but it is sometimes necessary. An ideal solution should support large files.

## Supported Systems

This solution must support git.

This solution should support subversion or migrations.

This solution could support TFSVC or migrations.

This solution could support Mercurial or migrations.

This solution won't support CVS.
