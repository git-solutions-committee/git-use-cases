# Accessibility

*Attn: Keith Hays for additional user stories*

## Mobility

I have mobility issues. This solution:
* must support logical keyboard navigation
* should not use deeply nested hover menus
* could support alternate input methods

## Neurological

*Placeholder* UI should be clearly labeled and logically arranged.

## Vision

I am visually impaired. This solution must:
* work with my screenreader
* work at alternate display sizes
* work with high/low contrast color schemes
