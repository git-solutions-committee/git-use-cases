# Access Management

## Active Directory Support

As project owners, we need a simple mechanism for updating access and project permissions, particularly for multi-unit teams. We must be able to use our existing campus identities for this purpose.

As a project manager, I must be able to grant project access by AD group so I don't have to manage individual NetIDs on a per-repo basis.

We have an internal project that we would like to open for review by trusted campus peers. We must be able to grant access using specialty AD groups (e.g., IT Pros).

## External Collaborators

Our development team spans several campus units and includes non-university contributors. We should be able to provide appropriate access to all of them without creating additional local accounts (e.g., using OAuth or similar).

## IT Support

One of our developers left without transferring repo ownership. This is code that should be owned by our unit. I need an approval process that grants me access.

As IT support staff, I occasionally need admin access to customer accounts and repos even though I am not part of their team or organizational structure.

Our support unit uses non-person AD accounts for much of its work. The chosen solution must support access by non-person AD accounts.

## Organizational Accounts

We have a large body of source code and project assets (e.g., icons, templates, etc.) that must be owned by our unit, not by individual developers. This is particularly important for data retention as team members join and leave our group.

Our unit's projects should be visible from our group page as a convenient way to point new users to our work.

We must be able to create separate teams within our organizational account in order to manage access appropriately.

Some of our projects are public and some are internal. We must be able to manage access on a repo-by-repo basis.

One of our research groups recently moved to a different department on campus. We must be able to transfer ownership of all of their project repositories to the new department.

## Team Management

Team management tools must be fast, lightweight, and easy to use. As a project owner, I must be able to manage my own team with a few clicks through the project dashboard. I won't have to email support just to find out who has access to the repo.
