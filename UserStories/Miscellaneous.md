# Miscellaneous

## API

This service must expose a rest api for us to integrate our other tools with it and import existing issues and repositories.

The ideal solution should have a robust API, allowing us to develop plugins or scripted tasks for missing functionality.

## Code Discovery

It would be helpful to have access to code samples vetted by campus domain experts. Common examples from the CCSP and UIUCWEBMASTERS mailing lists include LDAP config files for various platforms or accessible web components. Depending on their applicability, these samples could be made available to the general public or restricted to campus IT Professionals. They should ideally be hosted on a relevant organizational page, making them easily discoverable from the git service landing page.

Users should be able to search open project repositories for code samples, or be able to browse existing projects in a given language.

A campus project showcase (e.g., https://github.com/explore) could help IT Pros find code snippets and widgets more quickly.

## Cost

Spending approval is very limited in our group. Free or low-cost options are very important to us.

## Sensitive Data

Our unit has several groups working on medical research. Their datasets require special handling under HIPAA.

We handle student classwork and other data subject to FERPA restrictions.

*Placeholder* Our project has special requirements due to DOD funding.

## Static Site Generator

I write documentation for most of our projects. The documentation would be more usable displayed as webpages, but it's not worth maintaining a series of additional websites for this purpose. GitHub Pages or a similar static site generator serving pages at the repo level could solve this problem.

Our group already uses GitHub. We have a large body of internal documentation written for GitHub Pages. I would like to continue using the documentation after our projects are ported to the campus solution.
