# Migrations

Our group uses:
* Team Foundation Server
* Atlassian Jira, Jira Agile, Stash, and/or Crowd
* Public Bitbucket
* Public GitHub
* Redmine
* GitLab
* AITS subversion
* Locally hosted git repos
We need tools and documentation to help us migrate to the new service.
