# Campus Git Service 1-Pager

## Summary

The University of Illinois should launch a campuswide git service in order to model real world best practice and to remain competitive with its peers. This initiative requires strong backing from campus IT leadership in order to exercise due diligence, ensure interdepartmental cooperation, and authorize purchase.

## Background

Today's university environment runs on code. From coursework to administration to research and innovation, a robust version control system (VCS) is the bedrock tool in any software development workflow, and git-based version control is the industry standard[1]. Employers expect new Computer Science graduates to have experience using GitHub[2], a leading cloud-based git service. Our academic peers have incorporated git into their classrooms for years[3] [4]. Campus computing organizations offer git training alongside sessions on campus IT orientation, data management, and popular programming languages[5]. Campus IT Pros use git to manage their codebases and also as a learning tool, sharing code snippets and suggestions in order to benefit from each other's experience.

Despite git's ubiquity, the University of Illinois does not offer a campus- or university-level git-based version control service. The closest available option is AITS Subversion[6], which does not support git, nor does it offer the kinds of features commonly found and expected in popular, modern git services[7] [8] [9]. This void has fragmented the campus version control landscape. Though some units do use AITS subversion, many others find this an unsuitable solution. Some units purchase a license and host their own git service, duplicating costs across units; others use cloud-based vendor services, storing University assets offsite; still others, particularly small units and cross-campus affinity groups, simply do without. The net effect is an enormous cost in money, time, and opportunity, with substantial business continuity risks.
 
This document proposes that the University of Illinois launch a campus-wide, git-based version control service that supports modern social coding tools, licensed from a reputable vendor. A campus-wide git service would reduce duplicate spending, reduce duplication of effort in both service maintenance and code authoring, and ensure responsible stewardship of University resources, all while encouraging the cross-disciplinary collaboration that is the hallmark of modern software development.

## Benefits

**Student Success**

Familiarity builds fluency. By providing ready access to mainstream social coding tools, we give our students opportunities to practice critical collaborative technical skills while building the visible portfolios that employers want to see. 

**Technology for the Good of Campus**

Modern technology requires broad expertise, and social coding services help make non-programmers first-class participants in the development process. A campus-wide git-based version control service would ensure that the entire university community has access to today's tools. These services lower the barriers to technical participation, encouraging contributions from all team members and stakeholders thereby allowing cross-disciplinary teams to flourish. These services could greatly enhance the experience of our distance learning students, and will be critical for users of campus maker spaces and 3D-printing labs, to say nothing of more traditional users in engineering, computer science, and IT. Today's version control services underpin civic innovation, software development, and everything in between. Our campus users should reflect that trend.

**Teaching and Learning Innovation**

In keeping with social coding's move to the mainstream, service vendors are working to support the academic mission, offering coursework tools for educators[10], additional benefits for students[11], and training for new users. Many of our academic peers have established an official presence on popular git services[12], with notables including Harvard, Stanford, Indiana University, and Southern Illinois University.

**Research Support**

The University research community needs robust version control and collaboration tools, but is often bound by stringent access control requirements (e.g., HIPAA). We have an opportunity to support research innovation by hosting a storage solution tailored to research needs, combining verifiable data storage practices with easy-to-use, cross-institutional team management and visualization tools[13].

**Administrative Support & Campus Collaborations**

The University could realize immediate administrative support savings by selecting an off-the-shelf appliance from a trusted vendor. Campus units are already licensing and running near-identical services for their own use. By committing to the initial outlay and one-time configuration of a centralized service, we reduce not just the up-front costs of duplication, but the ongoing hidden wastes of staffing inefficiencies as well.

## Footnotes

1. Version Control Systems Popularity in 2016: https://rhodecode.com/insights/version-control-systems-2016.
2. R. Rutenbar, Department Head, Department of Computer Science, University of Illinois at Urbana-Champaign (personal communication, unverified)
3. L. Angrave. Senior Lecturer, Department of Computer Science. University of Illinois at Urbana-Champaign (personal communication, December 2, 2015)
4. GitHub Education Blog: https://github.com/blog/category/education
5. Research IT @ Illinois Training: https://publish.illinois.edu/researchit/training/coding-in-python-r-or-git/
6. AITS Subversion Home: https://web.uillinois.edu/subversion
7. GitLab Features: https://about.gitlab.com/features/
8. GitHub Features: https://github.com/features
9. Bitbucket Features: https://bitbucket.org/product/features
10. https://classroom.github.com/
11. https://education.github.com
12. Education organizations on GitHub: https://education.github.com/stories
13. GitHub + Jupyter Notebooks: https://github.com/blog/1995-github-jupyter-notebooks-3
