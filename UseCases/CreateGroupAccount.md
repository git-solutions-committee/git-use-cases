# Create A Group Account

## Summary
A new group account directory is created and made available through the service website.

## Rationale
The service account is a unique identifier that may correspond to individuals, campus units, or working groups. It is the primary means of managing and finding projects, groups, and people within the service. Group accounts (also Organization accounts) are similar to individual accounts except that they are typically shared among multiple users instead of used by an individual.

Group accounts provide high level access to projects, separation of projects, management of project team membership and access, and institutional data retention.

## Actors
All users

## Preconditions
1. Individual accounts corresponding to NetIDs are assumed to be prepopulated or created automatically on first login.
2. The user is logged in.

## Basic Flow
1. The website displays the user's account dashboard.
2. The user clicks the New Group link.
3. The website displays the New Group Options page
4. The user enters information about the project:
    * Group Name
    * Group Description
    * Additional options (e.g., initial members)
5. The website creates the Group account
    * Creates Group web directory under service root
    * Adds specified users to Group
    * Adds Group dashboard option to member user accounts
6. The website redirects the user to the new Group settings page.

## Alternate Flows
1. Insufficient Privilege: A process should exist for creating groups corresponding to campus units. In some cases, the user might be prompted to follow an alternate path to prove that they should own a departmental Group account.

## Exception Flows
1. Name Conflict: When leaving the Group Name field, the website will check for name conflicts within the service. If a conflict exists, the user will be prompted for a different name.

## Post Conditions
1. A web-accessible Group account directory is available and ready to be populated with teams and projects.
2. Access to project assets is granted to Group members.
3. The Group name appears in the group members' dashboards.
