# Create A Project

## Summary
A new project directory containing default/requested service features (e.g., git repository, issue tracker, etc.) is scaffolded in the owner's account and made available to authorized users.

## Rationale
This use case is a prerequisite for nearly all other service use cases. A project is a named instance of service features, providing users with a single location to manage file versions, access control, issues, and updates. Project names are unique to a given account, but may be repeated in different accounts. Projects may be owned by individual or organizational accounts. They may be fully public, fully private, or somewhere in between.

## Actors
All users

## Preconditions
1. A user account exists to contain the project.
2. The user is logged in.

## Basic Flow
1. The website displays the user's account dashboard.
2. The user clicks the New Project link.
3. The website displays the New Project Options page
4. The user enters information about the project:
    * Project Name
    * Project Description
    * Basic project visibility
    * Additional options (e.g., License, Readme, .gitignore)
5. The website initializes the project
    * Creates project web directory under owner's account
    * Creates source repository
    * Creates project-specific issue tracker
    * Scaffolds any default/optional files and features
    * Restricts access as specified
6. The website redirects the user to the new project page.

## Alternate Flows
1. At step 4, the user may specify an alternate owner account (e.g., a service organization or team to which the user belongs). The service will create the new project under the organization's account instead of the individual user's account.
2. At step 4, the user may be prompted to import an existing project from a supported selection of comparable services. At step 8, the project will be initialized, then populated using the repository and issues from the original project.

## Exception Flows
* Name Conflict: When leaving the Project Name field, the website will check for name conflicts within the Owner's account. If a conflict exists, the user will be prompted for a different name.
* Insufficient Access: When creating a project in an organizational account, a user may find that their access has been revoked. The website will warn of the error and return the user to the project creation screen, or will create the project with the user as owner.
* Disk Quota Reached: If storage quotas are in place, the user may be notified that they do not have sufficient storage to create a new project, and will be prompted to clear space in their account.

## Post Conditions
1. A web-accessible project directory is available under the Owner's account.
2. A git repository is ready to be populated with files.
3. A web-accessible issue tracker is ready to be populated with issues.
4. Any scaffolded files (e.g., Readme, license, .gitignore) are available in the project repository.
5. Access to project assets, including project existence and name, are restricted to the specified groups.
6. The project name appears in the project owner's dashboard.
