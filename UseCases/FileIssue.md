# File an Issue

## Summary
A unique issue with appropriate metadata (issue number, labels, milestones, author, etc.) and attachments is added to a project issue tracker.

## Rationale
Project issues are a project's to-do list. They may be used for anything from bug reports and enhancements to feature requests and change proposals. Project contributors must be able to browse or search existing issues (including closed issues), and should be able to filter by metadata (issue type, milestone, date, etc.).

With sufficient privilege contributors should be able to assign issues to users, add existing category labels, and add existing project milestones.

## Actors
1. All users with accounts
2. All users without accounts

## Preconditions
1. A project with an issue tracker
2. Sufficient access to view a project's issue tracker

## Basic Flow
1. The user opens the project issue page and clicks the New Issue link.
2. The website displays the New Issue page, with both Edit and Preview tabs.
3. The user enters information about the issue:
    * Issue Title
    * Issue Description: the user may reference other issues, lines of code, commits, users, etc.
    * Labels
    * Milestone
    * Assignee
4. The user may preview the rendered markdown using the Preview tab.
5. The website generates a unique issue number and adds the user-submitted information.
6. The website redirects the user to the new issue.

## Alternate Flows
1. The user may use an API to generate an issue programmatically. The service will send an appropriate response instead of redirecting to a webpage.
2. The issue tracker may allow public read, but authenticated write. The service will prompt for login at step 2.

## Exception Flows
1.

## Post Conditions
