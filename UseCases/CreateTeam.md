# Create A Project Team

## Summary
A new role, populated with members, is created within an account and may be used to modify access to a project or Group account. Team membership may be tied to campus AD or use external accounts. Team names are unique to an account, but may be repeated across the service (e.g., Developers).

## Rationale
In a sufficiently complex project or organization, project owners will want to be able to assign permissions by role instead of by individual user account. Role-based Teams simplify management and reduce risk by providing a central location in which to add and remove members from teams (e.g., an employee leaves the university), and teams from projects (e.g., an external vendor's contract has expired).

## Actors
1. All users
2. All Groups

## Preconditions
1. An individual or group account that will house the new team.
2. The user is logged in.

## Basic Flow
1. The website displays the user's account dashboard.
2. The user clicks the New Team link.
3. The website displays the New Team Options page
4. The user enters information about the project:
    * Team Name
    * Team Description
    * Team Members by account name or AD group
    * Additional privileges (e.g., Team Admin)
5. The website creates the Team
    * Creates Team populated with specified members
    * Adds Team to account Teams page
    * Lists number of projects with Team permissions
6. The website redirects the user to the new Team settings page.

## Alternate Flows

## Exception Flows
1. Name Conflict: When leaving the Group Name field, the website will check for name conflicts within the service. If a conflict exists, the user will be prompted for a different name.

## Post Conditions
1. A web-accessible Team directory is available to be modified from the owner's account.
2. A Team pseudo-account is available and may be used to modify project or Group account access.
